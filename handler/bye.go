package handler

import (
	"log"
	"net/http"
)

type Bye struct {
	l *log.Logger
}

func NewBye(l *log.Logger) *Bye {
	return &Bye{l}
}

func (h *Bye) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	h.l.Println("Bye world")
	// read the body

	// write the response
	rw.Write([]byte("Bye\n"))
	//fmt.Fprint(rw, "Bye")
}
