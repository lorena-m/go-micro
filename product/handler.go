package product

import (
	"log"
	"net/http"
	"regexp"
	"strconv"
)

type Handler struct {
	log    *log.Logger
	errLog *log.Logger
}

func NewHandler(log *log.Logger, e *log.Logger) *Handler {
	return &Handler{log, e}
}

func (h *Handler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		h.getProducts(rw, r)
	case http.MethodPost:
		h.postProduct(rw, r)
	case http.MethodPut:
		h.putProduct(rw, r)
	default:
		rw.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func (h *Handler) getProducts(rw http.ResponseWriter, _ *http.Request) {
	h.log.Println("handling GET products")
	lp := GetProducts()
	if err := lp.ToJson(rw); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
	}
}

func (h *Handler) postProduct(rw http.ResponseWriter, r *http.Request) {
	h.log.Println("handling POST products")

	p := &Product{}
	if err := p.FromJson(r.Body); err != nil {
		http.Error(rw, "Unmarshalling: "+err.Error(), http.StatusBadRequest)
	}
	AddProduct(p)
	h.log.Printf("Product: %#v", p)
}

func (h *Handler) putProduct(rw http.ResponseWriter, r *http.Request) {
	h.log.Println("handling PUT products")

	reg := regexp.MustCompile(`/([0-9]+)`)
	matches := reg.FindAllStringSubmatch(r.URL.Path, -1)
	if len(matches) != 1 || len(matches[0]) != 2 {
		http.Error(rw, "invalid URI "+r.URL.Path, http.StatusBadRequest)
		h.log.Println("regex doesn't match path:", r.URL.Path)
	}
	id, err := strconv.Atoi(matches[0][1])
	if err != nil {
		http.Error(rw, "invalid URI "+r.URL.Path, http.StatusBadRequest)
		h.log.Println("element does not represent an integer:", matches[0][1])
	}
	h.log.Printf("Received id %d", id)

	p := &Product{}
	if err := p.FromJson(r.Body); err != nil {
		http.Error(rw, "Unmarshalling: "+err.Error(), http.StatusBadRequest)
	}
	err = ReplaceProduct(id, p)
	h.log.Printf("Product: %#v", p)
	if err == ErrProductNotFound {
		http.Error(rw, "Product not found", http.StatusNotFound)
		return
	}
	if err != nil {
		http.Error(rw, "Product not found", http.StatusInternalServerError)
		return
	}
}
