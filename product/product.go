package product

import (
	"encoding/json"
	"fmt"
	"io"
	"time"
)

type Product struct {
	Id        int       `json:"id"`
	Title     string    `json:"title"`
	Author    string    `json:"author"`
	Price     float32   `json:"price"`
	CreatedOn time.Time `json:"-"`
	UpdatedOn string    `json:"-"`
	DeletedOn time.Time `json:"-"`
}

type Products []*Product

func (p *Product) FromJson(r io.Reader) error {
	e := json.NewDecoder(r)
	return e.Decode(p)
}

func (p *Products) ToJson(w io.Writer) error {
	e := json.NewEncoder(w)
	return e.Encode(p)
}

func GetProducts() Products {
	return productList
}

func AddProduct(p *Product) {
	p.Id = getNextId()
	productList = append(productList, p)
}

func ReplaceProduct(id int, p *Product) error {
	p, pos, err := findProduct(id)
	if err != nil {
		return err
	}
	productList[pos] = p
	return nil
}

var ErrProductNotFound = fmt.Errorf("Product not found")

func findProduct(id int) (*Product, int, error) {
	for i, p := range productList {
		if p.Id == id {
			return p, i, nil
		}
	}
	return nil, 0, ErrProductNotFound
}

func getNextId() int {
	return productList[len(productList)-1].Id + 1
}

var productList = Products{
	&Product{
		Id:        1,
		Title:     "Guards! Guards!",
		Author:    "Terry Pratchett",
		Price:     9.99,
		CreatedOn: time.Now(),
		UpdatedOn: time.Now().UTC().String(),
	},
}
