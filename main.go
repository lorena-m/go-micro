package main

import (
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/lorena-m/go-micro/product"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	l := log.New(os.Stdout, "[go-micro] ", log.LstdFlags)
	e := log.New(os.Stdout, "[go-micro] [ERROR] ", log.LstdFlags)
	ph := product.NewHandler(l, e)

	sm := mux.NewRouter()
	getRouter := sm.Methods("GET").Subrouter()
	sm.Handle("/products", ph)

	s := &http.Server{
		Addr:         ":9090",
		Handler:      sm,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	}

	// Listen for connections on all ip addresses (0.0.0.0)
	// port 9090
	l.Println("Starting server with timeouts")
	go func() {
		if err := s.ListenAndServe(); err != nil {
			e.Fatalln(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)

	// Block until a signal is received.
	sig := <-c
	l.Println("Got signal:", sig)

	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	tc, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	cancel()
	if err := s.Shutdown(tc); err != nil {
		e.Println(err)
	}
}
